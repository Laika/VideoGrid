package com.example.videogrid;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    public static final String EXTRA_MESSAGE = "com.example.videogrid.MESSAGE";
    public static final String EXTRA_MESSAGE1 = "com.example.videogrid.MESSAGE1";
    private static final int REQUEST_PERMISSION_WRITE = 1001;
    private static final String TAG = "MainActivityTAG";
    private List<VideoData> videoList;
    private GridView videoGrid;
    private GridAdapter adapter;
    private boolean permissionGranted;
    static final int REQUEST_VIDEO_CAPTURE = 1;

    @Override
    protected void onResume() {
        Log.i(TAG, "onResume() called");
        super.onResume();
        loadFromJSON();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "onCreate() called");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        checkPermissions();

        loadFromJSON();

        videoGrid = (GridView) findViewById(R.id.itemGrid);
        videoGrid.setAdapter(adapter);
        videoGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
//                Toast.makeText(MainActivity.this, "video: " + videoList.get(position).getName(),
//                        Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getApplicationContext(), ExoPlayerVideoActivity.class);
//                String VIDEO_ASSET = "skegss_lsd.mp4";
                String videoName = videoList.get(position).getName();
//                String path = "android.resource://" + getPackageName() + "/" + R.raw.skegss_lsd;
//                String path = "asset:///" + VIDEO_ASSET;
                String path = videoName;
                intent.putExtra(EXTRA_MESSAGE, path);
                intent.putExtra(EXTRA_MESSAGE1, videoName);
                startActivity(intent);
            }
        });
    }

    private void dispatchTakeVideoIntent() {
        Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        if (takeVideoIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takeVideoIntent, REQUEST_VIDEO_CAPTURE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == REQUEST_VIDEO_CAPTURE && resultCode == RESULT_OK) {
            Uri videoUri = intent.getData();
            String videoUriStr = videoUri.toString();
            VideoData video = new VideoData(null, videoUriStr, R.drawable.placeholder);
            Log.i(TAG, "onActivityResult() videoUri: " + videoUriStr);
            videoList.add(video);
            saveToJSON(videoList);
            adapter.notifyDataSetChanged();
            videoGrid.setAdapter(adapter);
            Toast.makeText(MainActivity.this, "Added: " + videoUriStr, Toast.LENGTH_SHORT).show();
        }
    }

    public void loadFromJSON() {
        Log.i(TAG, "loadFromJSON called");
        videoList = JSONVideos.importFromJSON(this);
        if (videoList != null) {
            adapter = new GridAdapter(this, videoList);
        } else {
            videoList = new ArrayList<>();
            adapter = new GridAdapter(this, videoList);
        }
    }

    public void saveToJSON(List<VideoData> videoList) {
        Log.i(TAG, "saveToJSON called");
        boolean result = JSONVideos.exportToJSON(this, videoList);
        if (result) {
            Toast.makeText(MainActivity.this, "Saved to storage.", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(MainActivity.this, "Failed to save to storage.", Toast.LENGTH_SHORT).show();
        }
    }

    public void onAddButtonClick(View view) {
        Log.i(TAG, "onAddButtonClick() called");
        Log.i(TAG, "permissionGranted: " + permissionGranted);
        Log.i(TAG, "checkPermission(): " + checkPermissions());
        if (!checkPermissions()) {
            return;
        }

        dispatchTakeVideoIntent();

//        EditText editText = (EditText) findViewById(R.id.addItemText);
//        String videoName = editText.getText().toString();
//        Log.i(TAG, "videoName: " + videoName);
//
//        if (videoName.equals("")) {
//            Toast.makeText(MainActivity.this, "Nothing added" + videoName,
//                    Toast.LENGTH_SHORT).show();
//        } else {
//            VideoData video = new VideoData(null, videoName, R.drawable.placeholder);
//            videoList.add(video);
//            saveToJSON(videoList);
//            editText.setText("");
//            adapter.notifyDataSetChanged();
//            videoGrid.setAdapter(adapter);
//            Toast.makeText(MainActivity.this, "Added: " + videoName, Toast.LENGTH_SHORT).show();
//
//        }
    }

    /* Checks if external storage is available for read and write */
    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state);
    }

    /* Checks if external storage is available to at least read */
    public boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        return (Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state));
    }

    // Initiate request for permissions.
    private boolean checkPermissions() {

        if (!isExternalStorageReadable() || !isExternalStorageWritable()) {
            Toast.makeText(this, "This app only works on devices with usable external storage",
                    Toast.LENGTH_SHORT).show();
            return false;
        }

        int permissionCheck = ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_PERMISSION_WRITE);
            return false;
        } else {
            return true;
        }
    }

    // Handle permissions result
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PERMISSION_WRITE:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    permissionGranted = true;
                    Toast.makeText(this, "External storage permission granted",
                            Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(this, "You must grant permission!", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }
}
