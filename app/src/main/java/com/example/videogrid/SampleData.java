package com.example.videogrid;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SampleData {
    public static List<VideoData> videoList;
    public static Map<String, VideoData> videoMap;

    static {
        videoList = new ArrayList<>();
        videoMap = new HashMap<>();
        addVideo(new VideoData(null, "Video 1", R.drawable.placeholder));
        addVideo(new VideoData(null, "Video 2", R.drawable.placeholder));
        addVideo(new VideoData(null, "Video 3", R.drawable.placeholder));
        addVideo(new VideoData(null, "Video 4", R.drawable.placeholder));
        addVideo(new VideoData(null, "Video 5", R.drawable.placeholder));
        addVideo(new VideoData(null, "Video 6", R.drawable.placeholder));
    }

    private static void addVideo(VideoData video) {
        videoList.add(video);
        videoMap.put(video.getId(), video);
    }
}
