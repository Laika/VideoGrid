package com.example.videogrid;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import com.google.gson.Gson;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

public class JSONVideos {
    private static final String FILE_NAME = "videos.json";
    private static final String TAG = "JSONVideos";

    public static boolean exportToJSON(Context context, List<VideoData> videoList) {
        Videos videoData = new Videos();
        videoData.setVideoList(videoList);

        Gson gson = new Gson();
        String jsonString = gson.toJson(videoData);
        Log.i(TAG, "exportToJSON: " + jsonString);

        FileOutputStream fileOutputStream = null;
        File file = new File(Environment.getExternalStorageDirectory(), FILE_NAME);

        try {
            fileOutputStream = new FileOutputStream(file);
            fileOutputStream.write(jsonString.getBytes());
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fileOutputStream != null) {
                try {
                    fileOutputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return false;
    }

    public static List<VideoData> importFromJSON(Context context) {
        FileReader reader = null;

        try {
            File file = new File(Environment.getExternalStorageDirectory(), FILE_NAME);
            reader = new FileReader(file);
            Gson gson = new Gson();
            Videos videoData = gson.fromJson(reader, Videos.class);
            return videoData.getVideoList();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return null;
    }

    static class Videos {
        List<VideoData> videoList;

        public List<VideoData> getVideoList() {
            return videoList;
        }

        public void setVideoList(List<VideoData> videoList) {
            this.videoList = videoList;
        }
    }
}
