package com.example.videogrid;

import java.util.UUID;

public class VideoData {
    private String id;
    private String name;
    private Integer thumbnail;

    public VideoData(String id, String name, Integer thumbnail) {

        if (id == null) {
            id = UUID.randomUUID().toString();
        }

        this.id = id;
        this.name = name;
        this.thumbnail = thumbnail;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(Integer thumbnail) {
        this.thumbnail = thumbnail;
    }

    @Override
    public String toString() {
        return "VideoData{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", thumbnail=" + thumbnail +
                '}';
    }
}
