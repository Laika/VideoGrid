package com.example.videogrid;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class GridAdapter extends BaseAdapter{

    public static final String VIDEO_ID_KEY = "video_id_key";
    private Context mContext;
    public List<VideoData> mVideos;

    public GridAdapter(Context context, List<VideoData> videos) {
        this.mContext = context;
        this.mVideos = videos;
    }

    @Override
    public int getCount() {
        return mVideos.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View videoGridView;
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final VideoData video = mVideos.get(i);

        if (view == null) {
            videoGridView = new View(mContext);
            videoGridView = inflater.inflate(R.layout.grid_layout, null);
            TextView textView = (TextView) videoGridView.findViewById(R.id.android_gridview_text);
            ImageView imageView = (ImageView) videoGridView.findViewById(R.id.android_gridview_image);
            textView.setText(video.getName());
            imageView.setImageResource(video.getThumbnail());

        } else {
            videoGridView = (View) view;
        }

        return videoGridView;
    }
}
